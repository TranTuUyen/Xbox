#include<string>
using namespace std;
class Folder{
   private :
	string nameFolder;
	string pathFolder;
   public :

	   string getName(){
	   
	     return nameFolder;
	   };

	   void setName(string str){
	      nameFolder = str;
	   }

	   string getPath(){
	     return pathFolder;
	   }

	   void setPath(string path){
	    pathFolder = path;
	   }
};