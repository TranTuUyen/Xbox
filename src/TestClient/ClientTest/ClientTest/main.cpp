﻿#include <iostream>
#include <WS2tcpip.h>
#include <string>
#include "Controller.cpp"

#pragma comment(lib, "ws2_32.lib")
using namespace std;

int status;
int chooser;

string pathUpload;
string pathDowndload;
string nameFileChoosed;
string extension;
string fileToUp;
Control* control;

void main()
{

    status = 1; // login
    // 2 : lay list file tu folder
    // 3 : send folder can mo
    // 7 : send action is upload
    string ipAdress = "192.168.56.1";
    int port = 5400;

    WSADATA data;

    WORD ver = MAKEWORD(2, 2);

    int wsResult = WSAStartup(ver, &data);

    if (wsResult != 0) {
        cout << "failed " << endl;
        return;
    }

    SOCKET sock = socket(AF_INET, SOCK_STREAM, 0);

    if (sock == INVALID_SOCKET) {
        cout << "cant create socker";
        return;
    }

    sockaddr_in hint;
    hint.sin_family = AF_INET;
    hint.sin_port = htons(port);
    inet_pton(AF_INET, ipAdress.c_str(), &hint.sin_addr);

    int connResult = connect(sock, (sockaddr*)&hint, sizeof(hint));
    if (connResult == SOCKET_ERROR) {
        closesocket(sock);
        WSACleanup();
        return;
    }

    char buf[4096];
    string userInput;

    do {

	//	 int byteReceived = recv(sock, buf, 4096, 0);
	//	 string msgFromSV = string(buf, 0, byteReceived);
	//	 if (msgFromSV.compare("yeu_cau_path") == 0){
	//	   cout << "\n can path de upload " << endl;
	//	 }
		// else
         if (status == 1) {
            cout << "Login tai khoan";
            cout << "nhap user va password <cach nhau boi dau cach >";
            getline(cin, userInput);

            // send
            if (userInput.size() > 0) {

                int sendResult = send(sock, userInput.c_str(), userInput.size() + 1, 0);
                if (sendResult != SOCKET_ERROR) {
                    ZeroMemory(buf, 4096);
                    int byteReceived = recv(sock, buf, 4096, 0);

                    if (byteReceived > 0) {
                        cout << string(buf, 0, byteReceived) << endl;

                        if (string(buf, 0, byteReceived).compare("OK Right") == 0) {

                            int k = control->showMenu();

                            if (k == 1) {

                                send(sock, "lf", 3, 0);
                                status = 2;
                            }

                            if (k == 2) {
                                cout << "khong thuc hien duoc tai day";
                            }
                        }
                        else {
                            cout << "Nothing to show";
                        }
                    }
                }
            }
        }
        else {

            if (status == 2) {
                int byteReceived = recv(sock, buf, 4096, 0);
                cout << "list file : " << string(buf, 0, byteReceived) << endl;

                string msgToServer = "open_f_";

                send(sock, msgToServer.c_str(), sizeof(msgToServer.c_str()) + 100, 0);
                status = 3;
            }
            else {
                if (status == 3) {
                    string ch;
                    cout << "chon folder : ";
                    cin >> ch;
                    if (ch.compare("create_new") == 0) {
                        send(sock, "nf", 3, 0);
                        status = 7;
                    }
                    else {
                      
                        send(sock, ch.c_str(), sizeof(ch.c_str()) + 100, 0);
                        status = 4;
                       
                    }
                }
                else {
                    if (status == 4) {

                        string ch;
                        int byteReceived = recv(sock, buf, 4096, 0);
                        cout << "list file tu folder : " << string(buf, 0, byteReceived);
                        cout << "\n nhap ten file can mo : ";
                        cin >> ch;
						nameFileChoosed = ch;
                        if (ch.compare("upload") == 0) {
                            send(sock, "up", 3, 0);
                            status = 11;
                        }
                        else {

                            if (ch.compare("sf") == 0) {
                                send(sock, "sf", 3, 0);
                                status = 9;
                            }
                            else {
                                send(sock, ch.c_str(), sizeof(ch.c_str()) + 100, 0);
                                status = 5;
                            }
                        }
                    }
                    else if (status == 5) {
                        int byteReceived = recv(sock, buf, 4096, 0);
                        cout << "reading file : " << string(buf, 0, byteReceived);
                        status = 6;
                    }
                    else {
                        if (status == 7) {
                            string nameFolder;
                            cin.ignore();
                            cout << "ten folder muon tao : " << endl;

                            getline(cin, nameFolder);

                            send(sock, nameFolder.c_str(), sizeof(nameFolder.c_str()) + 100, 0);
                            status = 8;
                        }
                        else if (status == 9) {
                            string nameFriend;
                            cin.ignore();
                            cout << "nhập tên username mà bạn muốn share : ";
                            getline(cin, nameFriend);
                            send(sock, nameFriend.c_str(), sizeof(nameFriend.c_str()) + 100, 0);
                            status = 10;
                        }
                        else {
                            if (status == 11) {

                                cin.ignore();
                                cout << "path cua file upload " << endl;
                                getline(cin, pathUpload);
                                send(sock, pathUpload.c_str(), sizeof(pathUpload.c_str()) + 100, 0);
                                status = 12;
                            }
                            else {
                                if (status == 12) {
                                    // gui noi dung di

									control->SplitFilename(pathUpload);

									if (extension.compare(".png") == 0){
										/*xu ly neu file send di la anh*/
										cout << "dang xu ly anh de upload " << endl;

									 

									
									}else{
									  ifstream infile(pathUpload);

                                    string content = "";
                                    int i;

                                    for (i = 0; infile.eof() != true; i++) // get content of infile
                                        content += infile.get();

                                    i--;
                                    content.erase(content.end() - 1); // erase last character

                                  
                                    infile.close();

                                    const char* cstr = content.c_str();
                                    send(sock, cstr, sizeof(cstr) + 1000, 0);
									}

                                    status = 13;
								} else
									if (status == 6){
									 string action;
									cout << "\n download [down] or delete [del]?";
									cin >> action;
									if (action.compare("down") == 0){
									  send(sock, "down", 1000, 0);
									  status = 14;
									}
                                   
									else
										if (action.compare("del") ==0){
											send(sock, "del", 1000, 0);
											status = 17;
										}else{
										   status = 16;
										
										}
									 
									
									
									}
								else{
									if (status == 14){
										// int byteReceived = recv(sock, buf, 15000, 0);
									//	 cout << "path to save download  file : " << string(buf, 0, byteReceived);

										 /*luu lai path de download*/
										 pathDowndload = "C:\\download\\" + nameFileChoosed + ".txt";
										
										 cout << "path download : " << pathDowndload << endl;
									     send(sock, "continue download...", 1000, 0);
									   
									     status = 15;
									}else{
										if (status == 15){
										    int byteReceived = recv(sock, buf, 15000, 0);
											 cout << "\n downloading file : " << string(buf, 0, byteReceived);

											 /*tao file va ghi du lieu vao*/

 
											 std::ofstream outfile(pathDowndload);

										   outfile << string(buf, 0, byteReceived);

                                             outfile.close();
									   	 status = 16;
										}
									}
								   
								}
                            }
                        }
                    }
                }
            }
        }

    } while (userInput.size() > 0);

    closesocket(sock);
    WSACleanup();
}